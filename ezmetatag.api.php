<?php

/**
 * @file
 * API hooks.
 */

/**
 * Alter metatag admin forms within ezmetatag.
 *
 * @param $form
 * @param $form_state
 * @param $entity_type
 * @param $bundle
 */
function hook_ezmetatag_form_HOOK(&$form, &$form_state, $entity_type, $bundle) {
  if ($entity_type) {
    $token_fields = array(
      'metatag:definition' => 'image_style:uri',
    );

    $lang = $form['metatags']['#language'];

    foreach ($token_fields as $field => $token_value) {
      $list = _ezmetatag_generate_token_list($entity_type, $bundle, $token_value);
      $element = $form['metatags'][$lang]['metatag-tab'][$field];
      _ezmetatag_form_show_possible_values($element, $form_state, $list);
    }
  }
}
