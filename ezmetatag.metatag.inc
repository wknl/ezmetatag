<?php

/**
 * Implements hook_metatag_info_alter().
 */
function ezmetatag_metatag_info_alter(&$info) {
  $hide_fields = array(

    // This can be done within browserconfig.xml.
    'msapplication-square310x310logo',
    'msapplication-square150x150logo',
    'msapplication-square70x70logo',
    'msapplication-wide310x150logo',
    'msapplication-tileimage',
    'msapplication-tilecolor',
    'msapplication-badge',
    'msapplication-notification',

    // Deprecated or ignored in many browsers or search engines.
    'MobileOptimized',
    'theme-color',
    'cleartype',
    'HandheldFriendly',
    'og:image:url',
    'content-language',
    'format-detection',
    'abstract',
    'geo.position',
    'geo.region',
    'geo.placename',
    'icbm',

    // Complex metatags that can be dealt in other ways
    'pragma',
    'cache-control',
    'expires',

    // Use the XML sitemap module.
    'revisit-after',
    'refresh',

    // Redundant with OpenGraph
    'image_src',
  );

  // No need for iOS7+
  if (variable_get('ezmetatag_disable_pre_ios7_favicons', 1)) {
    $hide_fields = array_merge($hide_fields, array(
      'apple-touch-icon-precomposed',
      'apple-touch-icon-precomposed_72x72',
      'apple-touch-icon-precomposed_76x76',
      'apple-touch-icon-precomposed_114x114',
      'apple-touch-icon-precomposed_120x120',
      'apple-touch-icon-precomposed_144x144',
      'apple-touch-icon-precomposed_152x152',
      'apple-touch-icon-precomposed_180x180',
    ));
  }

  // Sites don't uses these fields most of the time.
  if (variable_get('ezmetatag_disable_app_store', 1)) {
    $hide_fields = array_merge($hide_fields, array(
      'apple-itunes-app',
      'ios-app-link-alternative',
      'android-app-link-alternative',
      'twitter:app:country',
      'twitter:app:name:iphone',
      'twitter:app:id:iphone',
      'twitter:app:url:iphone',
      'twitter:app:name:ipad',
      'twitter:app:id:ipad',
      'twitter:app:url:ipad',
      'twitter:app:name:googleplay',
      'twitter:app:id:googleplay',
      'twitter:app:url:googleplay',
      'twitter:creator:id', // No twitter user knows this.
    ));
  }

  foreach ($hide_fields as $field) {
    if ($info['tags'][$field]) {
      unset($info['tags'][$field]);
    }
    else {
      watchdog('ezmetatag', 'Tag %title, no longer exists, update %function.', array(
        '%title' => $info['tags'][$field],
        '%function' => __FUNCTION__,
      ), WATCHDOG_NOTICE);
    }
  }

  // Deprecated or default robot metatags.
  $hide_robot_tags = array('noydir', 'index', 'follow');

  foreach($hide_robot_tags as $tag) {
    if (isset($info['tags']['robots']['form']['#options'][$tag])) {
      unset($info['tags']['robots']['form']['#options'][$tag]);
    }
    else {
      watchdog('ezmetatag', 'Robots tag %title, no longer exists, update %function.', array(
        '%title' => $tag,
        '%function' => __FUNCTION__,
      ), WATCHDOG_NOTICE);
    }
  }
}
