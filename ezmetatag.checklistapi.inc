<?php

/**
 * @file
 * Checklist API callbacks.
 */

/**
 * Implements hook_checklistapi_checklist_info().
 */
function ezmetatag_checklistapi_checklist_info() {
  $definitions = array();

  $definitions['ezmetatag'] = array(
    '#title' => t('Metatag Checklist'),
    '#path' => 'admin/config/search/metatags/checklist',
    '#description' => t('Add favicons for various browsers and operation systems.'),
    '#help' => t('<p>Use this checklist as a guide for configuring Metatags.</p>'),
    'basic' => array(
      '#title' => t('Basic'),
      'feature' => array(
        '#title' => t('Create metatag feature'),
        '#description' => l('Create feature', 'admin/structure/features/create'),
      ),
      'defaults' => array(
        '#title' => t('Disable <em>Load the module\'s default configurations</em>'),
        '#description' => l(t('Advanced Settings'), 'admin/config/search/metatags/settings'),
      ),
      'user' => array(
        '#title' => t('Remove <em>User</em> default metatag configuration and enable <em>Cache meta tag output</em>'),
        '#description' => t('Or setup User Metatag settings and disable metatag <em>Cache meta tag output</em>. See for more info: <a href="https://www.drupal.org/node/2852937">Metatag -Moderately Critical - Information disclosure - SA-CONTRIB-2017-019</a>'),
      ),
      'checklist' => array(
        '#title' => t('Save checklist variable in metatag feature'),
        '#description' => t('@feature: %config', array(
          '@feature' => 'Strongarm',
          '%config' => 'checklistapi_checklist_ezmetatag',
        )),
      ),
      'favicons' => array(
        '#title' => t('Generate and setup Favicons'),
        '#description' => l(t('Global Metatag configuration'), 'admin/config/search/metatags/config/global'),
      ),
      'browserconfig.xml' => array(
        '#title' => t('Check paths in <em>browserconfig.xml</em> and <em>manifest.json</em>'),
        '#description' => t('Can the paths be resolved and are they with a leading slash \'/\'?'),
      ),
      'permissions' => array(
        '#title' => t('Set permissions per metatag field'),
        '#description' => l(t('Metatag Permissions'), 'admin/people/permissions', array('fragment' => 'module-metatag')),
      ),
      'metatag' => array(
        '#title' => t('Save Metatag variables in metatag feature'),
        '#description' => t('@feature: %config', array(
          '@feature' => 'Strongarm',
          '%config' => 'metatag_*',
        )),
      ),
      'ezmetatag' => array(
        '#title' => t('Save ezMetatag variables in metatag feature'),
        '#description' => t('@feature: %config', array(
          '@feature' => 'Strongarm',
          '%config' => 'ezmetatag_*',
        )),
      ),
    ),
  );

  ctools_include('export');
  $configs = ctools_export_load_object('metatag_config');

  foreach ($configs as $config) {
    $parents = metatag_config_get_parent_instances($config->instance);
    list(, $parent) = $parents + array('', 'global');

    $definitions['ezmetatag'][$parent][$config->instance . ":config"] = array(
      '#title' => t('Configure <em>@label</em>', array(
        '@label' => metatag_config_title($config),
      )),
      '#description' => l(t('Configuration'), "admin/config/search/metatags/config/$config->instance"),
    );
    $definitions['ezmetatag'][$parent][$config->instance . ":feature"] = array(
      '#title' => t('Featurize <em>@label</em>', array(
        '@label' => metatag_config_title($config),
      )),
      '#description' => t('@feature: %config', array(
        '@feature' => 'Metatag',
        '%config' => $config->instance,
      )),
    );

    if (!isset($definitions['ezmetatag'][$parent]['#title'])) {
      $definitions['ezmetatag'][$parent]['#title'] = metatag_config_instance_label($parent);
    }
  }

  return $definitions;
}
